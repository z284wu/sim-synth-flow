# Synthesis testing for systemc-clang

The project structure mimics the aws hdk to allow for various simulators (and potentially various synthesis tools) to run.

Refer to [this document](cl/systemc-clang-examples/README.md) to see how to add tests.

# Directory Structure
```
cl/systemc-clang-examples: scripts and files for systemc-clang examples
common: commonly used tools/models for simulation and synthesis
```
