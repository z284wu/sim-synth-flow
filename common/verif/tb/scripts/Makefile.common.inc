ifeq ($(XSIM), 1)
  export SIMULATOR := xsim
else ifeq ($(VERILATOR), 1)
  export SIMULATOR := verilator
else ifeq ($(IVERILOG), 1)
  export SIMULATOR := iverilog
endif

include $(COMMON_DIR)/verif/tb/scripts/Makefile.$(SIMULATOR).inc

make_sim_dir:  # commonly used models
	mkdir -p $(SIM_ROOT)
