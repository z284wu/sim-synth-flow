// `timescale 1ns/1ps
/**
* tb_driver module is a manual translation of the following file:
* https://github.com/anikau31/systemc-clang/blob/master/examples/llnl-examples/zfpsynth/zfp2/test.cpp
*/
module tb_driver #(
  parameter I = 256,
  parameter O = 64
)(
  input logic clk,
  input logic reset,
  output logic        m_port_valid,
  input  logic        m_port_ready,
  output logic[O-1:0] m_port_data,

  input logic[I-1:0] s_port_data,
  input logic        s_port_valid,
  output logic       s_port_ready,

  output logic error
);
  localparam RLEVEL = 0;
  localparam DIMS = 1;
  localparam BLOCK_CNT = 2;
  localparam BLOCK_LEN = 1 << 2 * DIMS;
  logic[63:0] block[0:15];
  logic[31:0] scount;
  logic[31:0] rcount;
  logic c_sync_send;
  logic c_sync_recv;
  initial begin
    block[00] = 64'hbf7c3a7bb8495ca9;
    block[01] = 64'hbf79f9d9058ffdaf;
    block[02] = 64'hbf77c7abd0b61999;
    block[03] = 64'hbf75a42c806bd1da;
    block[04] = 64'hbf738f8f740b8ea8;
    block[05] = 64'hbf718a050399fef8;
    block[06] = 64'hbf6f2772ff8c30fe;
    block[07] = 64'hbf6b59aa63d22f68;
    block[08] = 64'hbf67aaf8b80cff9e;
    block[09] = 64'hbf641b9e71983592;
    block[10] = 64'hbf60abd3f723f2b7;
    block[11] = 64'hbf5ab7934169cc04;
    block[12] = 64'hbf54574f6f4897d3;
    block[13] = 64'hbf4c6e39da7fb99b;
    block[14] = 64'hbf40ae5826a893d1;
    block[15] = 64'hbf25bce8e19d48e1;
  end

  always @(*) begin : mc_send
    logic valid;
    logic sync;
    logic[O-1:0] flit;
    valid = scount < BLOCK_CNT * BLOCK_LEN;
    sync = m_port_ready && valid;
    flit = block[scount%BLOCK_LEN];
    m_port_data = flit;
    m_port_valid = valid;
    c_sync_send = sync;
    if(sync) begin
      $display("mc_send ts: %t, flit: %x:%x:%x", $time, flit[63], flit[52+:11], flit[0+:52]);
    end
  end

  always @(posedge clk) begin : ms_send
    if(reset == RLEVEL) begin
      scount <= 0;
    end else begin
      if(c_sync_send) begin
        scount <= scount + 1;
      end
    end
  end

  always @(*) begin : mc_recv
    logic sync;
    logic[I-1:0] flit;
    sync = s_port_valid;
    flit = s_port_data;
    s_port_ready = 1;
    error = rcount != BLOCK_CNT;
    c_sync_recv = sync;
    if(sync) begin
      $display("mc_recv ts: %t, flit: %x", $time, flit);
      if(flit != 256'hd4b7a6ff285c4c00d070a85e93ccce00cc0c4df4e004a200c78b088f6d46ae00) begin
        $error;
      end
    end
  end

  always @(posedge clk) begin : ms_recv
    if(reset == RLEVEL) begin
      rcount <= 0;
    end else begin
      if(c_sync_recv) begin
        rcount <= rcount + 1;
      end
    end
  end

  always @(posedge clk) begin
    if(reset != RLEVEL) begin
      // $display("tb_driver: m_port V: %b R: %b", m_port_valid, m_port_ready);
      // $display("tb_driver: s_port V: %b R: %b", s_port_valid, s_port_ready);
      if(c_sync_send) begin
        // $display("tb_driver: sent, scount: %d", scount);
      end
      if(c_sync_recv) begin
        // $display("tb_driver: recv, rcount: %d", rcount);
      end
    end
  end

endmodule

module test_sysc_zfp2
`ifdef VERILATOR
(
  input logic clock,
  input logic reset,
  output logic enc_error,
  output logic expo_error
)
`endif
;
  localparam DIMS = 1;
  localparam BLOCK_CNT = 2;
  localparam BLOCK_LEN = 1 << 2 * DIMS;
  `ifndef VERILATOR
  // verilator does not support delay so clock must be generated within the
  // c++ code
  logic clock;
  logic reset;
  logic enc_error;
  logic expo_error;

  initial begin

    $dumpvars;
    $timeformat (-9, 0, " ns", 5);
    clock = 0;
    reset = 1;
    #10 reset = 0;
    # 1100;
    // $display("enc error: ", enc_error);
    // $display("expo error: ", expo_error);
    $finish;
  end

  always clock = #5 !clock;

  `endif

  logic reset_n;
  assign reset_n = !reset;

  /* verilator lint_off PINMISSING */
  tb_driver #(.I(256), .O(64)) u_tb_driver(
    .clk(clock),
    .reset(reset_n),
    .error(expo_error)
  );

  `GENTOP top_module();
  /* verilator lint_on PINMISSING */

  /* NOTE: the signals are driven differently from ordinary ones */
  assign top_module.clk = clock;
  assign top_module.reset = reset_n;
  assign {
    top_module.c_driver_fp_data_sign,
    top_module.c_driver_fp_data_expo,
    top_module.c_driver_fp_data_frac
  } = u_tb_driver.m_port_data;
  assign top_module.c_driver_fp_valid = u_tb_driver.m_port_valid;
  assign u_tb_driver.m_port_ready = top_module.c_driver_fp_ready;
  assign u_tb_driver.s_port_data = top_module.c_dut_enc_data;
  assign u_tb_driver.s_port_valid = top_module.c_dut_enc_valid;
  assign top_module.c_dut_enc_ready = u_tb_driver.s_port_ready;


endmodule
