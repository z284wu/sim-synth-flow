// `timescale 1ns/1ps

module tb_send #(
  parameter T = 64
) (
  input logic clk,
  input logic reset,

  output logic        m_port_valid,
  input  logic        m_port_ready,
  output logic[T-1:0] m_port_data
);
  localparam RLEVEL = 0;
  localparam DIMS = 1;
  localparam BLOCK_CNT = 2;
  localparam BLOCK_LEN = 1 << 2 * DIMS;
  logic[63:0] block[0:15];
  logic[31:0] count;
  logic       c_sync;
  initial begin
    block[00] = 64'hbf7c3a7bb8495ca9;
    block[01] = 64'hbf79f9d9058ffdaf;
    block[02] = 64'hbf77c7abd0b61999;
    block[03] = 64'hbf75a42c806bd1da;
    block[04] = 64'hbf738f8f740b8ea8;
    block[05] = 64'hbf718a050399fef8;
    block[06] = 64'hbf6f2772ff8c30fe;
    block[07] = 64'hbf6b59aa63d22f68;
    block[08] = 64'hbf67aaf8b80cff9e;
    block[09] = 64'hbf641b9e71983592;
    block[10] = 64'hbf60abd3f723f2b7;
    block[11] = 64'hbf5ab7934169cc04;
    block[12] = 64'hbf54574f6f4897d3;
    block[13] = 64'hbf4c6e39da7fb99b;
    block[14] = 64'hbf40ae5826a893d1;
    block[15] = 64'hbf25bce8e19d48e1;
  end
  always @(*) begin : mc_send
    logic valid ;
    logic sync ; //m_port_ready && valid;
    logic[T-1:0] flit;

    valid = count < BLOCK_CNT * BLOCK_LEN;
    sync = m_port_ready && valid;
    flit = block[count % BLOCK_LEN];

    m_port_data = flit;
    m_port_valid = valid;
    c_sync = sync;
  end
  // assign m_port_valid = count < BLOCK_CNT * BLOCK_LEN;

  always @(posedge clk) begin: ms_send
    if(reset == RLEVEL) begin
      count <= 0;
    end else begin
      if(c_sync) begin
        $display("tb send: count %d -> %d", count, count + 1);
        count <= count + 1;
      end
    end
  end
endmodule

module tb_recv #(
  parameter T = 64,
  parameter CNT = -1
) (
  input logic clk,
  input logic reset,
  input logic[T-1:0] s_port_data,
  input logic        s_port_valid,
  output logic       s_port_ready,
  
  output logic error
);
  localparam RLEVEL = 0;
  localparam DIMS = 1;
  localparam BLOCK_CNT = 2;
  localparam BLOCK_LEN = 1 << 2 * DIMS;
  logic[31:0] count;
  logic c_sync;

  always @(*) begin: mc_recv
    logic sync;
    logic[T-1:0] flit;
    sync = s_port_valid;
    flit = s_port_data;
    s_port_ready = 1;
    error = count != CNT;
    c_sync = sync;
  end
  always @(posedge clk) begin: ms_recv
    if(reset == RLEVEL) begin
      count <= 0;
    end else begin
      if(c_sync) begin
        $display("tb recv: count %d -> %d (%d), data: %x", count, count + 1, CNT, s_port_data);
        count <= count + 1;
      end
    end
  end
endmodule

module test_sysc_zfp1
`ifdef VERILATOR
(
  input logic clock,
  input logic reset,
  output logic enc_error,
  output logic expo_error
)
`endif
;
  localparam DIMS = 1;
  localparam BLOCK_CNT = 2;
  localparam BLOCK_LEN = 1 << 2 * DIMS;
  `ifndef VERILATOR
  // verilator does not support delay so clock must be generated within the
  // c++ code
  logic clock;
  logic reset;
  logic enc_error;
  logic expo_error;

  initial begin
    clock = 0;
    reset = 1;
    #10 reset = 0;
    # 60;
    $display("enc error: ", enc_error);
    $display("expo error: ", expo_error);
    $finish;
  end

  always clock = #1 !clock;

  `endif

  logic reset_n;
  assign reset_n = !reset;

  /* verilator lint_off PINMISSING */
  tb_send u_tb_send(
    .clk(clock),
    .reset(reset_n)
  );
  tb_recv #(.CNT(BLOCK_CNT * BLOCK_LEN)) u_tb_recv_enc(
    .clk(clock),
    .reset(reset_n),
    .error(enc_error)
  );
  tb_recv #(.CNT(BLOCK_CNT)) u_tb_recv_expo(
    .clk(clock),
    .reset(reset_n),
    .error(expo_error)
  );

  `GENTOP top_module();
  /* verilator lint_on PINMISSING */

  /* NOTE: the signals are driven different from ordinary ones */
  assign top_module.clk = clock;
  assign top_module.reset = reset_n;
  assign {top_module.c_tb_send_fp_data_sign,
  top_module.c_tb_send_fp_data_expo,
  top_module.c_tb_send_fp_data_frac} =  u_tb_send.m_port_data;
  assign top_module.c_tb_send_fp_valid = u_tb_send.m_port_valid;
  assign u_tb_send.m_port_ready = top_module.c_tb_send_fp_ready;

  assign u_tb_recv_enc.s_port_data = {top_module.c_dut_enc_data_sign, top_module.c_dut_enc_data_expo, top_module.c_dut_enc_data_frac};
  assign u_tb_recv_enc.s_port_valid = top_module.c_dut_enc_valid;
  assign top_module.c_dut_enc_ready = u_tb_recv_enc.s_port_ready;

  assign u_tb_recv_expo.s_port_data = top_module.c_dut_expo_data;
  assign u_tb_recv_expo.s_port_valid = top_module.c_dut_expo_valid;
  assign top_module.c_dut_expo_ready = u_tb_recv_expo.s_port_ready;

endmodule
