`timescale 1ns/1ps
module test_sysc
`ifdef VERILATOR
(
  input logic clock,
  input logic reset
)
`endif
;
  `ifndef VERILATOR
  // verilator does not support delay so clock must be generated within the
  // c++ code
  logic clock;
  logic reset;
  initial begin
    clock = 0;
    reset = 1;
    #10 reset = 0;
  end
  always clock = #1 !clock;
  `endif

  logic reset_n;
  assign reset_n = !reset;

  `SYSC_TOP top_module(
    .clock(clock),
    .reset(reset),
    `SYSC_TOP_PORT
  );
endmodule
