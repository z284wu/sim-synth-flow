#include <iostream>
#include <verilated.h>
#ifndef TOPNAME
#error "Must provide top module name"
#endif

#define __stringify(x) #x
#define stringify(x) __stringify(x)
#include stringify(TOPHEADER)

TOPNAME *top = nullptr;

#ifdef TIMEOUT
vluint64_t time_out = TIMEOUT;
#else
vluint64_t time_out = 100;
#endif

vluint64_t main_time = 0;
double sc_time_stamp () { return main_time; }

// TODO: put this process into a separate header 
void loop_proc(bool& stop);

int main(int argc, char** argv) {
  Verilated::commandArgs(argc, argv);   // Remember args

  std::cout << "[ Verilator Testbench ]" << std::endl;

  top = new TOPNAME;
  top->reset = 0;
  top->clock = 0;

  while(main_time < time_out) {
    bool stop = false;
    loop_proc(stop);
    if(stop) break;
    if(main_time > 10) { top->reset = 0; }
    top->clock ^= 1;
    top->eval();
    main_time++;
  }
  top->final();
  delete top;
  return 0;
}
