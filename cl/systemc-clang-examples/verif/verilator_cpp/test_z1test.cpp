#include <iostream>
#include <cstdio>
#include <typeinfo>
#include <cassert>


#define __stringify(x) #x
#define stringify(x) __stringify(x)
#include stringify(TOPHEADER)

extern TOPNAME *top;
extern vluint64_t main_time;

void loop_proc(bool& stop) {
  if(main_time == 12) {
    top->enable = 1;
  } else if(main_time == 20) {
    std::cerr << "Test currently not implemented" << std::endl;
    stop = true;
  }
}
