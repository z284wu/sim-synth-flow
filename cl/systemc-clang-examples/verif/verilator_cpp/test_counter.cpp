#include <iostream>
#include <cstdio>
#include <typeinfo>


#define __stringify(x) #x
#define stringify(x) __stringify(x)
#include stringify(TOPHEADER)

extern TOPNAME *top;
extern vluint64_t main_time;

void loop_proc(bool& stop) {
  if(main_time == 12) {
    top->enable = 1;
  } else if(main_time == 20) {
    if(top->counter_out == 4) {
      std::cout << "Passed" << std::endl;
    } else {
      std::cout << "Failed " << "(actual counter_out=" << (int)top->counter_out << ")" << std::endl;
    }
    stop = true;
  }
}
