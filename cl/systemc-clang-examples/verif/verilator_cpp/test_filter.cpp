#include <iostream>
#include <cstdio>
#include <typeinfo>


#define __stringify(x) #x
#define stringify(x) __stringify(x)
#include stringify(TOPHEADER)

extern TOPNAME *top;
extern vluint64_t main_time;

void loop_proc(bool& stop) {
  if(main_time == 12) {
  } else if(main_time == 20) {
    std::cout << "Stopping" << std::endl;
    stop = true;
  }
}
