#include <iostream>

#define __stringify(x) #x
#define stringify(x) __stringify(x)
#include stringify(TOPHEADER)

extern TOPNAME *top;
extern vluint64_t main_time;

void loop_proc(bool& stop) {
  if(main_time == 12) {
    top->in_port_1 = 0;
    top->in_port_2 = 1;
  } else if(main_time == 14) {
    if(top->out_port == 0) {
      std::cout << "Passed" << std::endl;
    } else {
      std::cout << "Failed" << std::endl;
    }
    stop = true;
  }
}
