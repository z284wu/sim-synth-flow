# systemc-clang examples synthesis testing

## Overview

This example directory demonstrates how to use systemc-clang to translate cpp code to verilog, simulate the design and synthesize the design.

### Limitations
1. Currently, the design assumes a verilator simulator, thus the test code (with delays) will need to be written in C++. There might be a scope in integrating the SystemC testbench with verilator.
2. The Vivado simulator (xsim) is a work-in-progress.
3. The generated top-module must have a clock signal and a reset signal (active high).
4. The top-module name must be manually specified.
5. Currently, the automatic scan of examples are not supported due to the structure of zfp example. But we can formalize the structure so that it is eaiser to add auto-scan.

# Steps to add a design

## 1. Source the correct script
Source the `paths.sh` in the `systemc-clang` project.

Source the `envs.sh` in the root directory of the folder. If you are in the directory of this `README.md`, then:
```
> source ../../envs.sh
SYSTEMC_CLANG_BUILD_DIR: /home/allen/working/systemc-clang-build/
SYSTEMC_CLANG: /home/allen/working/systemc-clang/
```
Note that it should not report any error and print `SYSTEMC_CLANG_BUILD_DIR` and `SYSTEMC_CLANG` correctly.

## 2. The directory structure
There are four directories:
- `build`: only for building bitstreams
- `design`: for SystemC design files and other RTL verilog files
- `software`: for software that is executing on board or software runtime
- `verif`: for verification scripts and code, including Makefile that converts .cpp to .v

## 3. Add a SystemC design
To add an example, put the file in the `design/systemc/` folder. For example:
```
> mkdir -p design/systemc/and/ && touch design/systemc/and/and.cpp
```
And put the following code in `and.cpp`:
```c++
#include "systemc.h"
SC_MODULE(topand2) {
  sc_in_clk clock;
  sc_in<sc_uint<1> > reset;
  sc_in<int> in_port_1;
  sc_in<int> in_port_2;
  sc_out<int> out_port;

  SC_CTOR(topand2) {
    SC_METHOD(topEntry);
    sensitive<<clk.pos();
  }

  void topEntry() {
    if(reset.read()) {
      out_port.write(0);
    } else {
      out_port.write(in_port_1.read() & in_port_2.read());
    }
  }

};

int sc_main(int argc, char *argv[]){

  sc_clock CLOCK("clock", 5);
  sc_signal<sc_uint<1> > reset;
  sc_signal<int> input_1;
  sc_signal<int> input_2;
  sc_signal<int> output;
  topand2 t1 ("t1");

  t1.clock(CLOCK);
  t1.reset(reset);
  t1.in_port_1(input_1);
  t1.in_port_2(input_2);
  t1.out_port(output);

  sc_start(10, SC_NS);

  return 0;
}
```
> TODO: it is possible to copy the examples into the `design` folder or create separate rules for these designs
> TODO: it is possible to add design structure that has more than one files, for example, the `zfp` example, it is not tested and not documented. 

# 4. Create C++ test code
The verilator related C++ code is put in `verif/verilator_cpp` folder:
```
> ls verif/verilator_cpp/
main.cpp  test_xor.cpp
```
The `main.cpp` is the verilator C++ file that contains the main function and drives the clock/reset.
The `test_xor.cpp` file is the test for another design.

We create a `test_and.cpp` file:
```
> touch verif/verilator_cpp/test_and.cpp
```
The `test_and.cpp` has the following content:
```c++
#include <iostream>
#define __stringify(x) #x
#define stringify(x) __stringify(x)
#include stringify(TOPHEADER)
extern TOPNAME *top;
extern vluint64_t main_time;

void loop_proc(bool& stop) {
  if(main_time == 12) {
    top->in_port_1 = 0;
    top->in_port_2 = 1;
  } else if(main_time == 14) {
    if(top->out_port == 0) {
      std::cout << "Passed" << std::endl;
    } else {
      std::cout << "Failed" << std::endl;
    }
    stop = true;
  }
}
```
NOTE: 
1. the `TOPNAME` in the code will be a macro that is defined in the Makefile, which we go over later.
2. the `extern TOPNAME* top` is the pointer to the design in verilator.
3. the `extern vluint64_t main_time` is the time in verilator, each increment of `1` is a half-cycle.
4. the `loop_proc(bool& stop)` is a function that is called in every tick of the simulation, you can see its usage in the `verif/verilator_cpp/main.cpp`.
5. the `loop_proc` first does nothing in the first 6 cycles as that is the reset period. After that, we probe `in_port_1` and `in_port_2` and expects the result in `out_port` in the next cycle. We also stop the simulation after the check is done.
6. the `stringify` macros are hacks to get include filename from Makefile.

# 5. Add Makefile component
**Now change the directory to `verif/scripts`**:
```
cd verif/scripts
```
And we create the Makefile component for the `and` design (note that it is **`Makefrag.and`**, not `Makefile.and`):
```
> touch Makefrag.and
```
with the following content
```
SYSC_SRC=$(CL_ROOT)/design/systemc/and/and.cpp
SYSC_TOP=topand2_0
CXX_SRC:=$(CL_ROOT)/verif/verilator_cpp/test_and.cpp $(CXX_SRC)
```

- The `SYSC_SRC` is the SystemC clang source code for the design
- The `SYSC_TOP` is the top design that will be generated in the `_hdl.txt` file, currently, automatic top-module inference is not supported.
- The `CXX_SRC` is the verilator C++ file that we just added.
- TODO: currently, the suffix of `Makefrag.and`, the `.and` must be the same as the design name, can be resolved when adding support for arbitrary output filename.

It might be helpful to also review the content of `Makefile.verilator`:
```
CXX_SRC := $(CXX_SRC) $(CL_ROOT)/verif/verilator_cpp/main.cpp
V_SRC := $(SIM_DIR)/$(SYSC_TEST)_hdl.txt.v # $(CL_ROOT)/verif/tests/$(TEST).sv
VERILATOR_CFLAGS:=-DVERILATOR -g -std=c++14 -DTOPNAME=V$(SYSC_TOP) -DTOPHEADER=V$(SYSC_TOP).h
compile: gendesign
	mkdir -p $(SIM_DIR)
	verilator --Mdir $(SIM_DIR)/vprj \
		+1800-2017ext+sv \
			-CFLAGS "$(VERILATOR_CFLAGS)" \
			-LDFLAGS "-lpthread -ldl" \
			--top-module ${SYSC_TOP} \
			-sv \
			--cc \
			--exe $(V_SRC) $(CXX_SRC) \
			--trace \
			--trace-depth 99 \
			-DSYSC_TOP=${SYSC_TOP}
	make -C $(SIM_DIR)/vprj/ -j -f V$(SYSC_TOP).mk V$(SYSC_TOP)

run: compile
	$(SIM_DIR)/vprj/V$(SYSC_TOP)

clean:
	rm -rf $(SIM_DIR)/
```
Notice how the `SYSC_TOP` is inserted into the verilator command by supplying `-DTOPNAME` as C++ flags.

## The generation of `_txt.v` file
The rule for generating `_txt.v` file is in `Makefile`, note that it is calling the python script for generation:
```
$(SIM_DIR)/$(SYSC_TEST)_hdl.txt.v:
	# run the systemc-clang toolchain
	python ${SYSTEMC_CLANG}/tests/verilog-conversion/run-compare.py cpp-to-v \
    --cpp $(SYSC_SRC) \
    --output-dir $(SIM_DIR) \
    --no-ts \
    --force \
    --verbose

gendesign: $(SIM_DIR)/$(SYSC_TEST)_hdl.txt.v
```
> TODO: need a dedicated python script to act as the commandline tool entry or use native C++ for the conversion
> TODO: add support for specifying the output filename.

# 6. Compile and run verilator
Now run the verilator simulation:
```
> make -j1 all VERILATOR=1 SYSC_TEST=and
[ Verilator Testbench ]
Passed
```

The files generated by the systemc-clang is located at `../sim/verilator/test_sysc/and/` and the verilator project is located at `../sim/verilator/test_sysc/and/vprj`:
```
> tree ../sim
../sim
└── verilator
    └── test_sysc
        └── and
            ├── and_hdl.txt
            ├── and_hdl.txt.v
            ├── convert.py.stderr
            ├── convert.py.stdout
            ├── systemc-clang.stderr
            ├── systemc-clang.stdout
            └── vprj
                ├── main.d
                ├── main.o
                ├── test_and.d
                ├── test_and.o
                ├── verilated.d
                ├── verilated.o
                ├── verilated_vcd_c.d
                ├── verilated_vcd_c.o
                ├── Vtopand2_0
                ├── Vtopand2_0__ALL.a
                ├── Vtopand2_0__ALLcls.cpp
                ├── Vtopand2_0__ALLcls.d
                ├── Vtopand2_0__ALLcls.o
                ├── Vtopand2_0__ALLsup.cpp
                ├── Vtopand2_0__ALLsup.d
                ├── Vtopand2_0__ALLsup.o
                ├── Vtopand2_0_classes.mk
                ├── Vtopand2_0.cpp
                ├── Vtopand2_0.h
                ├── Vtopand2_0.mk
                ├── Vtopand2_0__Syms.cpp
                ├── Vtopand2_0__Syms.h
                ├── Vtopand2_0__Trace.cpp
                ├── Vtopand2_0__Trace__Slow.cpp
                ├── Vtopand2_0__ver.d
                └── Vtopand2_0__verFiles.dat

```

---

# Current status

Able to run throught the simulation with the `first-counter` and `xor` design with **hacks**.
For the `filter` example, there seems to be a senstivity list problem, from the `_hdl.txt` it seems hard to distinguish between sequential vs combinational.
The check for whether simulation result matches SystemC result is still work-in-progress.


The hacks are listed as follows:

- [ ]  Name of vardecl different from hmodule

Currently bypassing it in `convert.py` with a prefix match

```jsx
hModule first_counter_0

hVardecl counter [
  hTypeinfo  NONAME [
    hType first_counter NOLIST
  ]
]
```

- [ ]  Port Bindings for DUT is instantiate within submodule declaration instead of the parent module

```jsx

```

- [ ]  In `_XLAT_0`, the hTypeinfo represents `sc_bv` differently

```jsx

hVardecl _XLAT_0 [
  hTypeinfo  NONAME [
    hType sc_bv NOLIST
    hType 8 NOLIST
  ]
  hVarInit  NONAME [
    hSigAssignR read [
      hLiteral tap1 NOLIST
    ]
  ]
]
```

- [ ]  The function `sc_dtsc_proxysc_dtsc_bv_baseto_int` is not implemented in `avg.cpp` (hacked with Verilog `$sign`)
- [ ]  Variables declared in the middle of a process:

```jsx
prod1 = tmpTap1.to_int() * coef1;
prod2 = tmpTap2.to_int() * coef2;
prod3 = tmpTap3.to_int() * coef3;
...
int result2 = prod1 + prod2;
int result3 = prod1 + prod2 + prod3;
sum2.write(result2);
sum3.write(result3);
```

The definition together with its initial value are put at the beginning in _hdl.txt, however, values like prod1/prod2 are not ready there. Currently this is bypassed by manually putting declaration of result2/result3 outside.

- [ ]  Width conversion. for example: if `sum2` is an `sc_bv<8>`the `sum2.write(result2)` may generate code that narrows down the bit range. (Currently bypassed by disabling the warnings)

- [ ]  The sensitivity list seems a bit off as it seems to collect all sensitivity variables, currently I use `always @(posedge clock)` for all of methods, which is incorrect, but compiles and simulates.

```jsx
hSensvar create_method_process [
    hSensedge always NOLIST
  ]
  hSensvar pos [
    hSensedge pos NOLIST
  ]
  hSensvar sensitive [
    hSensedge always NOLIST
  ]
  hSensvar sensitive_neg [
    hSensedge always NOLIST
  ]
  hSensvar sensitive_pos [
    hSensedge always NOLIST
  ]
  hSensvar tap1 [
    hSensedge always NOLIST
  ]
  hSensvar tap2 [
    hSensedge always NOLIST
  ]
  hSensvar tap3 [
    hSensedge always NOLIST
  ]
]
```
