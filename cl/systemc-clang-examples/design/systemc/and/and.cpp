#include "systemc.h"
SC_MODULE(topand2) {
  sc_in_clk clock;
  sc_in<sc_uint<1> > reset;
  sc_in<int> in_port_1;
  sc_in<int> in_port_2;
  sc_out<int> out_port;

  SC_CTOR(topand2) {
    SC_METHOD(topEntry);
    sensitive<<clk.pos();
  }

  void topEntry() {
    if(reset.read()) {
      out_port.write(0);
    } else {
      out_port.write(in_port_1.read() & in_port_2.read());
    }
  }

};

int sc_main(int argc, char *argv[]){

  sc_clock CLOCK("clock", 5);
  sc_signal<sc_uint<1> > reset;
  sc_signal<int> input_1;
  sc_signal<int> input_2;
  sc_signal<int> output;
  topand2 t1 ("t1");

  t1.clock(CLOCK);
  t1.reset(reset);
  t1.in_port_1(input_1);
  t1.in_port_2(input_2);
  t1.out_port(output);

  sc_start(10, SC_NS);

  return 0;
}
