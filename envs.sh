export ROOT=$(cd $(dirname ${BASH_SOURCE[0]}); pwd -P)
export COMMON_DIR=$ROOT/common
[ -z "$SYSTEMC_CLANG_BUILD_DIR" ] && echo "Need to set SYSTEMC_CLANG_BUILD_DIR (source the corresponding systemc-clang paths.sh)" && exit 1;
echo SYSTEMC_CLANG_BUILD_DIR: $SYSTEMC_CLANG_BUILD_DIR
[ -z "$SYSTEMC_CLANG" ] && echo "Need to set SYSTEMC_CLANG (source the corresponding systemc-clang paths.sh)" && exit 1;
echo SYSTEMC_CLANG: $SYSTEMC_CLANG
